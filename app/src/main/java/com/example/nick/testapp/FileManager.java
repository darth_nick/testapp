package com.example.nick.testapp;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class FileManager {
    private static final String TAG = "testApp File manager";

    FileManager(Context context) {
        Log.d(TAG, "constructor called");
    }

    public boolean writeFile(String s,String filename,Context ctx) {
        Log.d(TAG, "Writing:"+s);
        try {
            FileOutputStream fOut = ctx.openFileOutput(filename, Context.MODE_WORLD_READABLE);

            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            // записываем строку в файл
            osw.write(s);
            /* проверяем, что все действительно записалось и закрываем файл */
            osw.flush();
            osw.close();
            return true;
            }
        catch(IOException e)
        {
            e.printStackTrace();
            Log.d(TAG, "exception in write file");
        }
        return false;
    }
    public String readFile(String filename,Context ctx) {
        Log.d(TAG, "reading file:"+filename);
        try {
            InputStream inputStream = ctx.openFileInput(filename);


            if (inputStream != null) {
                InputStreamReader isr = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(isr);
                String line;
                StringBuilder builder = new StringBuilder();

                while ((line = reader.readLine()) != null) {
                    builder.append(line + "\n");
                }

                inputStream.close();
                Log.d(TAG, "red from file:" +builder.toString());
                return builder.toString();
            }
        } catch(IOException e){
            e.printStackTrace();
            Log.d(TAG,"exception in read file");
        }
        return null;
    }
    public String readAssetsFile(String filename,Context ctx) {
        Log.d(TAG, "reading assets file:"+filename);
        try {
            AssetManager am = ctx.getAssets();
            InputStream inputStream = am.open(filename);
                    //ctx.openFileInput(filename);

            if (inputStream != null) {
                InputStreamReader isr = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(isr);
                String line;
                StringBuilder builder = new StringBuilder();

                while ((line = reader.readLine()) != null) {
                    builder.append(line + "\n");
                }

                inputStream.close();
                Log.d(TAG, "red from assets file:" +builder.toString());
                return builder.toString();
            }
        } catch(IOException e){
            e.printStackTrace();
            Log.d(TAG,"exception in assets read file");
        }
        return null;
    }
}
