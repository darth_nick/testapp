package com.example.nick.testapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.app.Activity;

public class DBactivity extends Activity {
    private SQLiteDatabase mSqLiteDatabase;
    private DatabaseHelper mDatabaseHelper;
    private static final String TAG = "testApp";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.db_layout);
        Log.d(TAG, "DBActivity oncreate");
        mDatabaseHelper = new DatabaseHelper(this, "mydatabase2.db", null, 1);
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();
    }

    public void dbGet(View v) {
        Log.d(TAG, "DBActivity onClick");
        Cursor cursor = mSqLiteDatabase.query("cats", new String[] {DatabaseHelper._ID,
                        DatabaseHelper.CAT_NAME_COLUMN, DatabaseHelper.PHONE_COLUMN,
                        DatabaseHelper.AGE_COLUMN}, null, null, null, null, null) ;

        cursor.moveToFirst();
        while(!cursor.isLast()) {
            int id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper._ID));
            String catName = cursor.getString(cursor.getColumnIndex(DatabaseHelper.CAT_NAME_COLUMN));
            int phoneNumber = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.PHONE_COLUMN));
            int age = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.AGE_COLUMN));

            TextView infoTextView = (TextView) findViewById(R.id.textView);
            infoTextView.append(id+" Кот " + catName + " имеет телефон " + phoneNumber + " и ему " +
                    age + " лет");
        cursor.moveToNext();
        }
        // не забываем закрывать курсор
        cursor.close();
    }

    public void dbFill(View v)
    {
        Log.d(TAG, "DBActivity onFill");
        ContentValues newValues = new ContentValues();
        // Задайте значения для каждого столбца
        newValues.put(DatabaseHelper.CAT_NAME_COLUMN, "Рыжик");
        newValues.put(DatabaseHelper.PHONE_COLUMN, "4954553443");
        newValues.put(DatabaseHelper.AGE_COLUMN, "5");
        // Вставляем данные в таблицу
        mSqLiteDatabase.insert("cats", null, newValues);
    }

    public void dbDrop(View v)
    {
        Log.d(TAG, "DBActivity onDrop");
        mSqLiteDatabase.execSQL("DROP TABLE IF EXISTS '" + "cats" + "'");
        mDatabaseHelper = new DatabaseHelper(this, "mydatabase2.db", null, 1);
        mSqLiteDatabase = mDatabaseHelper.getWritableDatabase();
        mDatabaseHelper.onCreate(mSqLiteDatabase);
        TextView infoTextView = (TextView) findViewById(R.id.textView);
        infoTextView.setText("");
        dbFill(v);
    }

    public void fileButton(View v)
    {
        final String filename ="data.txt";
        FileManager fMan=new FileManager(this);
        String answer = fMan.readAssetsFile(filename,this);
        fMan.writeFile(answer,filename, this);
        fMan.readFile(filename,this);
    }
}
