package com.example.nick.testapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.EditText;


public class MainActivity extends Activity {
    private Intent intentsuccess;
    private Intent intentfail;
    private final String pass="qwe";
    private static final String TAG = "testApp";

    private boolean pswrdCheck(String s)
    {
        return s.equals(pass);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "Main oncreate");

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        Button exit = (Button) findViewById(R.id.buttonExit);
        OnClickListener oclExit = new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
        exit.setOnClickListener(oclExit);


        intentfail = new Intent(this, ScreenFail.class);
        intentsuccess = new Intent(this, ScreenSuccess.class);

        Button btn= (Button) findViewById(R.id.verifyButton);

        OnClickListener oclBtn = new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.pwrdEdit);
                System.out.println(editText.getText().toString());

                if(pswrdCheck(editText.getText().toString())==true)

                    startActivity(intentsuccess);
                else
                    startActivity(intentfail);
            }
        };

        btn.setOnClickListener(oclBtn);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
