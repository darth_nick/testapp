package com.example.nick.testapp;

import android.os.Bundle;
import android.app.Activity;

public class ScreenFail extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fail);
    }
}
