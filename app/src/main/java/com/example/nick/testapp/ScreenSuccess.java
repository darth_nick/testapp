package com.example.nick.testapp;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.app.Activity;
import android.widget.Button;

public class ScreenSuccess extends Activity {
    private static final String TAG = "testApp";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        Log.d(TAG, "Succ oncreate called");


           FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
           fab.setOnClickListener(new View.OnClickListener()

           {
               @Override
               public void onClick (View view){
               Snackbar.make(view, "Going back", Snackbar.LENGTH_LONG)
                       .setAction("Action", null).show();
               onBackPressed();
           }
           }

           );


       }
    public void dbDrop() {
        //дропает таблицу
        Log.d(TAG, "Succ dbDrop called");
    }

    public void onDBClick(View v) {
        Log.d(TAG, "Succ onDBClick called");
        Intent intenttoDB;
        intenttoDB = new Intent(this, DBactivity.class);
        startActivity(intenttoDB);
    }

}
